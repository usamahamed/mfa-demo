# MFA-Demo
The Demo idea come from this interesting article https://medium.com/onfrontiers-engineering/two-factor-authentication-flow-with-node-and-react-7cbdf249f13

# Run the demo app
- Install dependencies: npm install
- Build the app: npm run build:dev
- Start the server: npm run server:dev

